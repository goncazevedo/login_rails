class LikesController < ApplicationController
    before_action :find_post

    def create
        if !has_liked?
            @post.likes.create(user_id: current_user().id)
        end
        redirect_to post_path(@post)
    end


    private

    

        def has_liked?
            Like.where(user_id: current_user().id, post_id: params[:post_id]).exists?
        end

        def find_post
            @post = Post.find(params[:post_id])
        end

end
