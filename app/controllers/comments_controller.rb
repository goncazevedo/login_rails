class CommentsController < ApplicationController
    before_action :find_post

    def create
        @comment = Comment.new(comment_params)
        if @comment.save
            redirect_to post_path(@post)

        else
            redirect_to root_path

        end
    end

    private

        def comment_params
            params.permit(:user_id, :post_id, :content)
        end


        def find_post
            @post = Post.find(params[:post_id])
        end

end
